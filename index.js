// Bài 1: Nhập vào 3 số. Viết chương trình xuất ra 3 số theo thứ tự tăng dần
document.getElementById("sapXep").onclick = function () {
  // lấy thông tin từ ô input
  var num1 = document.getElementById("txtNum1").value * 1;
  var num2 = document.getElementById("txtNum2").value * 1;
  var num3 = document.getElementById("txtNum3").value * 1;
  //   các bước xử lý
  // 1. Tạo biến chứa kết quả
  var ketQua = "";

  //  so sánh các số trong ô input với nhau qua các trường hợp:
  // th1:
  if (num1 > num2 && num1 > num3) {
    if (num2 > num3) {
      ketQua = num3 + " < " + num2 + " < " + num1;
    }
    if (num2 < num3) {
      ketQua = num2 + " < " + num3 + " < " + num1;
    }
  }
  //   th2:
  if (num2 > num1 && num2 > num3) {
    if (num1 > num3) {
      ketQua = num3 + " < " + num1 + " < " + num2;
    }
    if (num1 < num3) {
      ketQua = num1 + " < " + num3 + " < " + num2;
    }
  }
  // th3:
  if (num3 > num1 && num3 > num2) {
    if (num1 > num2) {
      ketQua = num2 + " < " + num1 + " < " + num3;
    }
    if (num1 < num2) {
      ketQua = num1 + " < " + num2 + " < " + num3;
    }
  }
  // show kết quả lên giao diện
  document.getElementById("result1").innerHTML = ketQua;
};

// Bài 2: Viết chương trình chào hỏi thành viên trong gia đình
document.getElementById("xinChao").onclick = function () {
  // lấy giá trị trong ô option của select và xét cho từng trường hợp
  var cauChao = document.getElementById("loiChao").value;
  if (cauChao == "B") {
    cauChao = "Xin Chào Bố";
  } else if (cauChao == "M") {
    cauChao = "Xin Chào Mẹ";
  } else if (cauChao == "A") {
    cauChao = "Xin Chào Anh";
  } else if (cauChao == "E") {
    cauChao = "Xin Chào Em Gái";
  } else {
    cauChao = "Xin Chào Bạn";
  }
  // swhow kết quả lên giao diện
  document.getElementById("result2").innerHTML = cauChao;
};

// Bài 3: Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số chẵn, lẻ?
document.getElementById("demChanLe").onclick = function () {
  // lấy thông tin từ ô input
  var soThuNhat = document.getElementById("txtSo1").value * 1;
  var soThuHai = document.getElementById("txtSo2").value * 1;
  var soThuBa = document.getElementById("txtSo3").value * 1;
  // các bước xử lý
  // 1. tạo biến chứa kết quả đếm số chẵn, đếm số lẻ
  var demSoChan = 0;
  var demSoLe = 0;
  // 2. Xét các trường hợp: Nếu chia hết cho 2 là số chẵn, ngược lại là số lẻ
  if (soThuNhat % 2 === 0) {
    demSoChan = demSoChan + 1;
  } else {
    demSoLe = demSoLe + 1;
  }
  if (soThuHai % 2 === 0) {
    demSoChan = demSoChan + 1;
  } else {
    demSoLe = demSoLe + 1;
  }
  if (soThuBa % 2 === 0) {
    demSoChan = demSoChan + 1;
  } else {
    demSoLe = demSoLe + 1;
  }
  // 3. tạo biến chứa kết quả
  var ketQua = "";
  ketQua = "có " + demSoChan + " số chẵn và có " + demSoLe + " số lẻ";
  //   show kết quả lên giao diện
  document.getElementById("result3").innerHTML = ketQua;
};

// Bài 4: Viết chương trình nhập vào 3 cạnh của một tam giác. Hãy cho biết dó là tam giác gì?
document.getElementById("timTamGiac").onclick = function () {
  // lấy thông tin ở ô input
  var canh1 = document.getElementById("txtCanh1").value * 1;
  var canh2 = document.getElementById("txtCanh2").value * 1;
  var canh3 = document.getElementById("txtCanh3").value * 1;
  // các bước xử lý
  // tạo biến chứa kết quả tam giác tìm được
  var tamGiac = "";
  // xét các trường hợp của tam giác
  if (canh1 == canh2 && canh1 != canh3 && canh2 != canh3) {
    tamGiac = "Tam giác cân";
  } else if (canh1 == canh3 && canh1 != canh2 && canh2 != canh3) {
    tamGiac = "Tam giác cân";
  } else if (canh2 == canh3 && canh2 != canh1 && canh3 != canh1) {
    tamGiac = "Tam giác cân";
  } else if (canh1 == canh2 && canh1 == canh3 && canh2 == canh3) {
    tamGiac = "Tam giác đều";
  } else if (canh1 * canh1 + canh2 * canh2 == canh3 * canh3) {
    tamGiac = "Tam giác cân";
  } else if (canh1 * canh1 + canh3 * canh3 == canh2 * canh2) {
    tamGiac = "Tam giác cân";
  } else if (canh2 * canh2 + canh3 * canh3 == canh1 * canh1) {
    tamGiac = "Tam giác cân";
  } else {
    tamGiac = "Một loại tam giác khác";
  }
  // show kết quả lên giao diện
  document.getElementById("result4").innerHTML = tamGiac;
};
